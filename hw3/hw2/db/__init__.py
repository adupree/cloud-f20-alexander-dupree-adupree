"""
    :module: db

    :brief: Package defines both abstract and concrete database implementations.

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from enum import Enum, auto
from flask import g, current_app

from db.mock_db import Mock_DB
from db.sqlite3_db import Sqlite3_DB


class DBMS(Enum):
    ''' Enumerates supported DBMS dialects in the application '''
    Sqlite3     =   auto()
    Postgres    =   auto()
    Mock        =   auto()


def get_db():
    '''Returns the current DB connection in the application context'''
    if 'db' not in g:
        g.db = init_db(current_app.config)
    return g.db


def init_db(config):
    ''' DB Model factory, returns the concrete DB model based on config'''

    db_connectors = {
        DBMS.Sqlite3    :   Sqlite3_DB,
        DBMS.Postgres   :   Mock_DB,
        DBMS.Mock       :   Mock_DB
    }

    return db_connectors[config.get('DBMS', DBMS.Sqlite3)](config)
    