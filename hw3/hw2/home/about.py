"""
    :module: home.about

    :brief: Presenter for about.html

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from flask import render_template
from flask.views import MethodView

class About(MethodView):
    """
    Class-based presentor object for about.html, implemented as a pluggable
    view. 
    """

    def get(self):
        '''renders the index.html view'''
        return render_template('about.html', title='About us')
