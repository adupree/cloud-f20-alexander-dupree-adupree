"""
    :module: home.index

    :brief: Presenter for index.html, landing page for the Charity Connect site

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from flask import render_template
from flask.views import MethodView

class Index(MethodView):
    """
    Class-based presentor object for index.html, implemented as a pluggable
    view. 
    """

    def get(self):
        '''renders the index.html view'''
        return render_template('index.html')
