@echo off
:: File     :   runner.bat
:: Brief    :   batch file to setup virtualenv and run application
:: Author   :   Alexander DuPree
:: https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2

:: Check Dependencies
py -3 --version > nul 2>&1
if %ERRORLEVEL% NEQ 0 (
    echo "Error: Python3 is not installed or accessible via %%PATH%%"
    exit /B %ERRORLEVEL%
)

pip --version > nul 2>&1
if %ERRORLEVEL% NEQ 0 (
    echo "Error: pip is not installed or accessible via %%PATH%%"
    exit /B %ERRORLEVEL%
)

:: Create virtual environment
if not exist %cd%\env (
    py -3 -m venv env
)

:: Activate virtual environment and install dependencies
if not defined VIRTUAL_ENV (
    CALL .\env\Scripts\activate
)

pip install -r requirements.txt > nul 2>&1

:: Run application
python app.py

exit