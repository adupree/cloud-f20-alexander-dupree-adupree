"""
    :module:    app.py

    :brief:     Entry point for the Charity Connect web application

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from os import sys

from flask import Flask, g

from db import init_db
from utils import config, parser
from home.routes import home
from charities.routes import charities

app = Flask(__name__)

app.register_blueprint(home)
app.register_blueprint(charities)

@app.teardown_appcontext
def teardown_db(exception):
    """Closes open DB resources if it exists
    """
    db = g.pop('db', None)

    if db is not None:
        db.close()

if __name__ == '__main__':

    args = parser.parse(sys.argv[1:])

    app.config.from_object(args['config'])

    init_db(app.config)

    app.run(host=args['hostname'], port=args['port'], debug=args['debug'])