"""
    :module: charities.forms

    :brief: Module defines the form class required for registering charities

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

import re
from flask_wtf import FlaskForm
from flask.views import MethodView
from flask import render_template, url_for, flash, redirect
from wtforms import SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, Email, Length, ValidationError

from db import get_db
from charities.models import Charity, Service

class RegistrationForm(FlaskForm):
    '''WTForm for gathering data and performing validation on input for Charity
    Registration page
    '''
    name        = StringField('Charity Name', validators=[DataRequired(), Length(min=2, max=40)])
    description = StringField('Description')
    email       = StringField('Email', validators=[DataRequired(), Email()])
    service     = SelectField('Service', choices=Service.service_list)
    phone       = StringField('Phone')
    submit      = SubmitField('Submit Application')

    def validate_name(self, name):
        db = get_db()
        if db.get_charity(name.data) is not None:
            raise ValidationError('A Charity with that name already exists')

    def validate_phone(self, phone):
        pattern = re.compile('^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$')
        if pattern.fullmatch(phone.data) is None:
            raise ValidationError('Please enter a valid phone number in the form: 123-456-7890')


class Register(MethodView):
    ''' Presentor for register.html '''

    def get(self):
        form = RegistrationForm()
        return render_template('register.html', title='Register Charity', form=form)

    def post(self):
        db = get_db()
        form = RegistrationForm()
        if form.validate_on_submit():
            charity = Charity( form.name.data
                             , form.description.data
                             , form.email.data 
                             , form.service.data
                             , form.phone.data )
            db.add_charity(charity)
            flash('Charity Application Submitted!', 'success')
            return redirect(url_for('charities.list'))
        return render_template('register.html', title='Register Charity', form=form)
