"""
    :module: charities.list

    :brief: Presenter for list.html.

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from flask import render_template
from flask.views import MethodView

from db import get_db

class List(MethodView):
    """
    Class-based presentor object for index.html, implemented as a pluggable
    view. 
    """

    def get(self):
        '''Gather charities into a list and renders them with list.html'''
        db = get_db()
        charities = db.get_all_charities()
        return render_template('list.html', title='Charity List', charities=charities)

