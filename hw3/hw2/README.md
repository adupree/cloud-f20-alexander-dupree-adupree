# Homework #2

Scalable web application utilizing the MVP architecture and Flask web framework.

## Getting Started

Download this repo:

```
git clone https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree.git
```

From the hw2 directory setup the virtual environment and run:

```
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt
python app.py
```

This will bring up an instance of the web app on `localhost:500`. Alternatively, I've provided convenience scripts for setting up and running the application:

```
./runner.sh
```
or for windows
```
.\runner.bat
```

Currently there isn't much in the way of application configuration but there is a minimal command line parser with different options. 
Run `python app.py -h` for a list of options.
