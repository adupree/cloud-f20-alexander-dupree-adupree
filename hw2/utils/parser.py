"""
    :module: utils.parser

    :brief: Implements command line parsing utility, useful for testing 
        different configurations from the CLI. concrete configurations should
        be defined in the utils.config module

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

import argparse


def parse(argv):
    """Parsers command line arguments into a dictionary structure

    :param argv: list of arguments, e.g. sys.argv[1:]

    :returns: A dictionary with command line options as keys and provided
        arguments as values
    """

    parser = argparse.ArgumentParser(
        description="Web application that lists charities and social services"
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5000,
        help="Port number for the web server, default=5000"
    )
    parser.add_argument(
        '-n', '--hostname',
        default='0.0.0.0',
        help='Hostname to listen on, default=0.0.0.0'
    )
    parser.add_argument(
        '-d', '--debug',
        default=False,
        action='store_true',
        help="Enable flask debug mode"
    )
    parser.add_argument(
        '-c', '--config',
        default='utils.config.Development',
        help='Flask configuration file'
    )
    return vars(parser.parse_args(argv))
