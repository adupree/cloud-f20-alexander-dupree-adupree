"""
    :module: utils.config

    :brief: Defines different application configurations as class objects

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

import os
from db import DBMS

class Config(object):
    '''Base configuration'''
    SECRET_KEY  = os.environ.get('SECRET_KEY', os.urandom(12)) 
    DEBUG       = False
    TESTING     = False

class Development(Config):
    ''' Development configuration '''
    DEBUG   =   True
    DBMS    =   DBMS.Sqlite3
    DB_URI  =   'cc_dev.db'
    ENV     =   'development'

class Production(Config):
    ''' Production configuration '''
    DBMS    =   DBMS.Sqlite3
    DB_URI  =   'cc_prod.db'
    ENV     =   'production'