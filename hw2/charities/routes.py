"""
    :module:    charities.routes

    :brief:     Blueprint for the charities module of the Charity Connect 
        web application

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from flask import Blueprint
from charities.list import List
from charities.register import Register

charities = Blueprint('charities', __name__)

charities.add_url_rule('/charities/list', view_func=List.as_view('list'))
charities.add_url_rule('/charities/register', view_func=Register.as_view('register'))
