"""
    :module:    db.models

    :brief:     Abstract databse models definitions

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from abc import ABC, abstractmethod

class DB_Model(ABC):
    '''Abstract database model class

    The DB_Model defines a uniform interface for reading, writing, and
    updating application models
    '''


    def __init__(self, config):
        self.config = config
        pass

    @abstractmethod
    def add_charity(self, charity):
        pass


    @abstractmethod
    def get_charity(self, charity_name):
        pass


    @abstractmethod
    def get_all_charities(self):
        pass


    @abstractmethod
    def delete_charity(self, charity_id):
        pass


    @abstractmethod
    def close(self):
        pass
