"""
    :module:    app.py

    :brief:     Entry point for the imagegraph web application

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from urllib.parse import unquote
from os import environ, urandom, path

from dotenv import load_dotenv
from flask_login import LoginManager, current_user, login_required
from werkzeug.exceptions import HTTPException, InternalServerError
from flask import Flask, flash, g, render_template, redirect, request, url_for, session 

import google_auth
from db import get_db, init_db
from db.models import Entity, User

# [APPLICATION SETUP] TODO: Move this into an application factory pattern

# Load application settings
load_dotenv(dotenv_path='app.env')

# Load secret keys
load_dotenv(dotenv_path=environ.get('SECRETS_LOC'))

app = Flask(__name__)

app.register_blueprint(google_auth.app)

login_manager = LoginManager(app)

app.config['SECRET_KEY']            =   environ.get('SECRET_KEY')
app.config['DBMS']                  =   environ.get('DBMS')
app.config['DEBUG']                 =   (environ.get('DEBUG') == 'true')
app.config['ENV']                   =   environ.get('ENV')
app.config['PORT']                  =   environ.get('PORT', default='5000')
app.config['HOSTNAME']              =   environ.get('HOSTNAME', default='0.0.0.0')
app.config['MAX_CONTENT_LENGTH']    =   int(environ.get('MAX_CONTENT_LENGTH'))

init_db(app.config)

# [ROUTES]
@app.route('/')
def index():
    '''Landing page for the Image graph application'''

    db = get_db()

    images = []
    labels = {}
    user = google_auth.get_current_user()
    if user and user.is_authenticated:
        images = db.get_images(user.id)
        for image in images:
            for ident in image['label_ids']:
                labels[ident] = db.get_entity_data(ident)

    return render_template('index.html', images=images, labels=labels, user=user)


@app.route('/upload_photo', methods=['POST'])
@login_required
def upload_photo():
    '''Endpoint allowing user to upload photos for analysis. Photos are stored 
    on the DB
    '''

    db = get_db()

    image = request.files['file']

    uri = db.upload_image(image)

    flash('Image Uploaded Successfully!', 'success')
    return redirect(url_for('analyze', img_uri=uri))


@app.route('/analyze')
@login_required
def analyze():
    '''Endpoint makes an API call to Google Vision to annotate the image and
    then feeds the annotated labels into the Knowledge Graph API. The resulting
    analysis is stored on the DB
    '''

    db = get_db()
    img_uri = unquote(request.args.get('img_uri'))

    labels = detect_labels_uri(img_uri)
    label_analysis = knowledge_graph_search(labels)

    db.store_analysis(current_user.get_id(), img_uri, label_analysis)
    return redirect(url_for('index'))


# [API CALLERS]
def detect_labels_uri(uri):
    """Detects labels in the file located in Google Cloud Storage or on the
    Web.
    
    Args:
        uri (str): uri for the image to be analyzed

    Returns:
        list: A list of label annotations from the analysis
    """
    from google.cloud import vision

    client = vision.ImageAnnotatorClient()
    image = vision.Image()
    image.source.image_uri = uri

    response = client.label_detection(image=image)
    labels = response.label_annotations

    if response.error.message:
        flash(response.error.message, 'warning')

    return labels


def knowledge_graph_search(labels):
    """Conduct a knowledge graph search for each label in labels

    Args:
        labels (list): List of label annotations (output from detect_labels_uri)

    Returns:
        list: A list of db.models.Entity objects
    """

    from googleapiclient import discovery
    kgsearch = discovery.build( # Create kgsearch api client
        'kgsearch',
        'v1',
        developerKey=environ.get('KG_API_KEY'),
        cache_discovery=False
    )

    entities = []
    for label in labels:

        # Conduct Knowledge graph search
        req = kgsearch.entities().search(ids=label.mid, limit=1)
        res = req.execute()

        # Parse results and store into entity structure
        if res and res.get('itemListElement') is not None and len(res['itemListElement']) > 0:
            entity          =   res['itemListElement'][0]['result']
            name            =   entity.get('name', '')
            description     =   entity.get('description', '')
            detailed_desc   =   entity.get('detailedDescription', {})
            img_url         =   entity.get('image', {}).get('contentUrl')
            url             =   detailed_desc.get('url')
            article         =   detailed_desc.get('articleBody')

            entities.append( Entity(
                name,
                description, 
                article,
                img_url,
                url, 
                label.mid
            ) )
    return entities


# [SUBROUTINES]
@login_manager.user_loader
def load_user(user_id):
    '''Grabs the user matching the user_id from the database. This method is 
    Required for the Flask Login Manager to manage sessions properly.
    
    Args: 
        user_id (str): unicode id for the db.models.User object 
    Returns:
        db.models.User: User matching the user_id 
    '''
    db = get_db()
    user = db.get_user(user_id)

    # New user encountered, add user to DB
    if user is None and google_auth.is_logged_in():
        user = google_auth.get_current_user()
        db.add_user(user)
    return user


@app.errorhandler(HTTPException)
@app.errorhandler(InternalServerError)
def handle_exception(err):
    ''' General error handler for flask application '''
    flash(f'{err.code}-{err.name}: {err.description}', 'danger')
    return redirect(url_for('index'))


@app.teardown_appcontext
def teardown_db(exception):
    """ Closes any open DB resources """
    db = g.pop('db', None)

    if db is not None:
        db.close()


if __name__ == '__main__':

    app.run(host=app.config['HOSTNAME'], port=app.config['PORT'], debug=app.config['DEBUG'])
