"""
    :module:    google_auth

    :brief:     Implements a Flask blueprint for handling Oauth2 login with Google

    :author:    Alexander DuPree

    :credit:    https://www.mattbutton.com/2019/01/05/google-authentication-with-python-and-flask/
"""

import os
import functools

import google.oauth2.credentials
import googleapiclient.discovery
from authlib.client import OAuth2Session

from flask_login import login_user, logout_user
from flask import Blueprint, make_response, request, redirect, session

from db.models import User

ACCESS_TOKEN_URI = 'https://www.googleapis.com/oauth2/v4/token'
AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent'

AUTHORIZATION_SCOPE ='openid email profile'

AUTH_TOKEN_KEY = 'auth_token'
AUTH_STATE_KEY = 'auth_state'

app = Blueprint('google_auth', __name__)


def is_logged_in():
    ''' Checks if the Oauth token key is present in the current session '''
    return True if AUTH_TOKEN_KEY in session else False

def build_credentials():
    '''Builds the current users oauth credentials. Needed in order to make API
    calls requesting the users info on their behalf. 
    '''
    client_id = os.environ.get('GOOGLE_CLIENT_ID')
    client_secret = os.environ.get('GOOGLE_CLIENT_SECRET')

    if not is_logged_in():
        raise Exception('User must be logged in')

    oauth2_tokens = session[AUTH_TOKEN_KEY]
    
    return google.oauth2.credentials.Credentials(
                oauth2_tokens['access_token'],
                refresh_token=oauth2_tokens['refresh_token'],
                client_id=client_id,
                client_secret=client_secret,
                token_uri=ACCESS_TOKEN_URI)

def get_user_info():
    '''Returns a dictionary object containing the current users info'''

    credentials = build_credentials()

    oauth2_client = googleapiclient.discovery.build(
                        'oauth2', 'v2',
                        credentials=credentials)

    return oauth2_client.userinfo().get().execute()


def get_current_user():
    '''Returns the current users info packaged into the db.models.User object'''
    if not is_logged_in():
        return None

    user_info = get_user_info()
    return User(user_info['id'], user_info['email'], user_info['name'])


def no_cache(view):
    '''Function decorator to apply the cache-control headers to outgoing responses
    '''
    @functools.wraps(view)
    def no_cache_impl(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return functools.update_wrapper(no_cache_impl, view)

@app.route('/google/login')
@no_cache
def login():
    '''Entrypoint for the google Oauth process. Creates an oauth session and 
    redirects the client to the authorization url so they can login to their 
    google account
    '''
    base_uri = request.host_url
    auth_redirect_uri = base_uri + 'google/auth'
    client_id = os.environ.get('GOOGLE_CLIENT_ID')
    client_secret = os.environ.get('GOOGLE_CLIENT_SECRET')

    oauth_session = OAuth2Session(
        client_id=client_id, 
        client_secret=client_secret, 
        scope=AUTHORIZATION_SCOPE, 
        redirect_uri=auth_redirect_uri
        )
  
    uri, state = oauth_session.create_authorization_url(AUTHORIZATION_URL)

    session[AUTH_STATE_KEY] = state
    session.permanent = True

    return redirect(uri, code=302)

@app.route('/google/auth')
@no_cache
def google_auth_redirect():
    ''' Oauth callback route is reached after the user authenticates with google. 
    We retrieve the request state and build and store the required oauth2 tokens
    into our session. Lastly we use the Flask Login manager login_user function 
    to handle session management
    '''
    base_uri = request.host_url
    auth_redirect_uri = base_uri + 'google/auth'
    client_id = os.environ.get('GOOGLE_CLIENT_ID')
    client_secret = os.environ.get('GOOGLE_CLIENT_SECRET')

    req_state = request.args.get('state', default=None, type=None)

    if req_state != session[AUTH_STATE_KEY]:
        response = make_response('Invalid state parameter', 401)
        return response
    
    oauth_session = OAuth2Session(
        client_id=client_id, 
        client_secret=client_secret,
        scope=AUTHORIZATION_SCOPE,
        state=session[AUTH_STATE_KEY],
        redirect_uri=auth_redirect_uri
        )

    oauth2_tokens = oauth_session.fetch_access_token(
                        ACCESS_TOKEN_URI,            
                        authorization_response=request.url
                        )

    session[AUTH_TOKEN_KEY] = oauth2_tokens

    login_user(get_current_user())

    return redirect(base_uri, code=302)

@app.route('/google/logout')
@no_cache
def logout():
    ''' Pops the oauth token and state off our session and alerts Flask login 
    manager that the user has logged out.
    '''
    base_uri = request.host_url

    session.pop(AUTH_TOKEN_KEY, None)
    session.pop(AUTH_STATE_KEY, None)

    logout_user()

    return redirect(base_uri, code=302)
