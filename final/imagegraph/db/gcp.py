"""
    :module: db.gcp

    :brief: Implements the database model as a Google Cloud Datatstore and 
        storage buckets

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw4
"""

from os import environ
from uuid import uuid4
from datetime import datetime

from google.cloud import datastore
from google.cloud import storage
from werkzeug.utils import secure_filename

from db.models import DB_Model, Entity, User

class GCP(DB_Model):
    ''' Database model implementation with a Google Cloud backend'''

    LABEL_KIND = 'Labels'
    IMAGE_KIND = 'Images'
    USER_KIND  = 'Users'


    def __init__(self, config):
        '''Constructor instantiates a GCP datastore client'''
        super(GCP, self).__init__(config)
        self.project_id         =   environ.get('PROJECT_ID')
        self.bucket_name        =   environ.get('CLOUD_STORAGE_BUCKET')
        self.datastore_client   =   datastore.Client(self.project_id)
        self.storage_client     =   storage.Client(self.project_id)


    def upload_image(self, image):
        '''Uploads image data to a cloud storage bucket and makes it publicly
        accessible

        Args:
            image (FileStorage): File storage object containing image data

        '''

        bucket = self.storage_client.get_bucket(self.bucket_name)

        # Clean the filename and make it unique before uploading it
        filename = str(uuid4().hex) + '_' + secure_filename(image.filename)

        blob = bucket.blob(filename) # TODO Research maximum filename length 
        blob.upload_from_string(image.read(), content_type=image.content_type)

        blob.make_public()

        return blob.public_url

    
    def store_analysis(self, user_id, img_uri, analysis):
        ''' Stores image graph analysis onto the cloud datastore 

        Args:
            user_id (str): Unique user identifier for the user who uploaded the 
                           photo
            img_uri (str): URI to access the image in future loads
            analysis (list): List of db.models.Entity objects 
        '''
        img_key                 =   self.datastore_client.key(GCP.IMAGE_KIND, img_uri)
        img_entity              =   datastore.Entity(img_key)
        img_entity['uri']       =   img_uri
        img_entity['timestamp'] =   datetime.now()
        img_entity['user_id']   =   user_id
        img_entity['label_ids'] =   [ entity.mid for entity in analysis ]

        self.datastore_client.put(img_entity)

        for label in analysis:
            label_key = self.datastore_client.key(GCP.LABEL_KIND, label.mid)
            entity = datastore.Entity(label_key)
            entity.update(label.__dict__)
            self.datastore_client.put(entity)
        # TODO Error handling and reporting

    def get_entity_data(self, ident):
        '''Grabs previous knowledge grpah analysis data from the datastore

        Args:
            ident (str): Machine ID of the analzyed entity

        Returns:
            db.models.Entity or None
        '''
        key = self.datastore_client.key(GCP.LABEL_KIND, ident)
        entity = self.datastore_client.get(key)

        if not entity: 
            return None
        
        return Entity(
            name=entity['name'],
            desc=entity['desc'],
            article=entity['article'],
            img_url=entity['img_url'],
            url=entity['url'],
            mid=entity['mid']
        )
        


    def get_images(self, user_id):
        ''' Get all images for a specific user 
        Args:
            user_id (db.models.User.id)

        Returns:
            list: List of dicts containing image data
            # TODO Package image data into a object relational model
        '''

        query = self.datastore_client.query(kind=GCP.IMAGE_KIND)
        query.add_filter('user_id', '=', user_id)

        return list(query.fetch())


    def get_user(self, user_id):
        ''' Grabs the user matching the user_id from the cloud datastore
        Args: 
            user_id (str): id attribute from db.models.User.id

        Returns:
            db.models.User or None
        '''
        key = self.datastore_client.key(GCP.USER_KIND, user_id)
        user = self.datastore_client.get(key)

        if not user:
            return None
        
        return User(user['id'], user['email'], user['name'])


    def add_user(self, user):
        ''' Adds a new user or Updates the user in the cloud datastore
        Args: 
            user (db.models.User): User object to add or update
        '''
        key = self.datastore_client.key(GCP.USER_KIND, user.id)
        entity = datastore.Entity(key)

        entity.update(user.__dict__)
        self.datastore_client.put(entity)
    

    def close(self):
        '''No teardown needed for GCP Datastore'''
        return
