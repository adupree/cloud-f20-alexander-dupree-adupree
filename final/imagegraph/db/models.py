"""
    :module:    db.models

    :brief:     Abstract database models definitions

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from abc import ABC, abstractmethod
from flask_login import UserMixin

class DB_Model(ABC):
    '''Abstract database model class

    The DB_Model defines a uniform interface for reading, writing, and
    updating application models
    '''

    def __init__(self, config):
        self.config = config
        pass


    @abstractmethod
    def upload_image(self, photo):
        pass

    
    @abstractmethod
    def store_analysis(self, user_id, img_uri, analysis):
        pass


    @abstractmethod
    def get_entity_data(self, ident):
        pass


    @abstractmethod
    def get_images(self, user_id):
        pass
    

    @abstractmethod
    def get_user(self, user_id):
        pass


    @abstractmethod
    def add_user(self, user):
        pass


    @abstractmethod
    def close(self):
        pass

class Entity:
    ''' Entity object packages the Knowledge Graph API search results into a 
    serializable data structure
    '''

    def __init__(self, name, desc, article, img_url, url, mid):
        self.name       =   name
        self.desc       =   desc
        self.article    =   article
        self.img_url    =   img_url
        self.url        =   url
        self.mid        =   mid # Machine ID

        if name and desc:
            self.title = f'{name}: {desc}'
        else:
            self.title = name


class User(UserMixin):
    ''' User object inherits the UserMixin base class for default use with the
    Flask Login manager
    '''

    def __init__(self, id, email, name):
        self.id         =   id
        self.email      =   email
        self.name       =   name
