"""
    :module: db

    :brief: Package defines both abstract and concrete database implementations.

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from flask import g, current_app

from db.gcp import GCP
from db.sqlite3_db import Sqlite3_DB


class DBMS:
    ''' Enumerates supported DBMS dialects in the application '''
    Sqlite3     =   'sqlite3'
    Datastore   =   'datastore'


def get_db():
    '''Returns the current DB connection in the application context'''
    if 'db' not in g:
        g.db = init_db(current_app.config)
    return g.db


def init_db(config):
    ''' DB Model factory, returns the concrete DB model based on config'''

    db_connectors = {
        DBMS.Sqlite3    :   Sqlite3_DB,
        DBMS.Datastore  :   GCP,
    }

    return db_connectors[config.get('DBMS', DBMS.Sqlite3)](config)
    
