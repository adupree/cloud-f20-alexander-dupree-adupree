"""
    :module: db.sqlite3_db

    :brief: Implements the database model as a sqlite3 database

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

import sqlite3
from db.models import DB_Model


# TODO finish implementing Sqlite3 backend
class Sqlite3_DB(DB_Model):
    ''' Database model implementation with a sqlite3 backend'''

    def __init__(self, config):
        '''Constructor connects to the database and initializes the tables
        if needed
        '''

        super(Sqlite3_DB, self).__init__(config)

        self.conn = sqlite3.connect( 
            config.get('DB_URI', ':memory:'),
            detect_types=sqlite3.PARSE_DECLTYPES
            )

        self.conn.row_factory = sqlite3.Row

        with open('db_schema.sql') as f:
            self.conn.executescript(f.read())


    def upload_image(self, image):
        pass

    
    def store_analysis(self, user_id, img_uri, analysis):
        pass


    def get_entity_data(self, ident):
        pass


    def get_images(self, user_id):
        pass
    

    def get_user(self, user_id):
        pass


    def add_user(self, user):
        pass


    def close(self):
        '''Closes the DB connection'''
        self.conn.commit()
        self.conn.close()
