![logo](imagegraph/static/images/logo_small.png)

# ImageGraph - CS430P Final Project


## Introduction

The ImageGraph web application is an educational project designed to better understand cloud technologies. This application leverages the Google cloud platform to perform 
image label annotation and analysis with the Vision and Knowledge Graph APIs. 

See a live example of ImageGraph [Here!](https://imagegraph-nodvqymjcq-uw.a.run.app)

Other technologies powering this project:
* [Flask Web Framework](https://flask.palletsprojects.com/en/1.1.x/)
* [Gunicorn WSGI HTTP Server](https://gunicorn.org/)
* [Docker](https://www.docker.com/)
* [Oauth2](https://oauth.net/2/)
* [Google Cloud Datastore](https://cloud.google.com/datastore)
* [Google Cloud Storage](https://cloud.google.com/storage)
* [Google Cloud Run](https://cloud.google.com/run)
* [Sqlite3](https://www.sqlite.org/index.html)

## Future Project Goals

Right now Imagegraph is a minimal CRUD app without the U or D. In the future I would like to expand the functionality of the application to include: 

* Add Update and Delete operations
* The vision analysis is currently a blocking operation, it would be better to offload this to a cloud function or worker thread. 
* Add advertisements to the site to offset deployment costs
* Add a support and donate page. 
* Clean up the code

## Prerequisites

Universal Requirements: 

* Google account and a [Storage Bucket](https://cloud.google.com/storage/docs/creating-buckets)
* [Knowledge Graph API Key](https://developers.google.com/knowledge-graph/how-tos/authorizing). 
* Google Cloud Service account credentials with Storage.Admin and Datastore.User roles. See this [article](https://cloud.google.com/iam/docs/service-account)
* Oauth2 Client Credentials for [Google](https://developers.google.com/identity/protocols/oauth2)

Running the app with Python:

* Python 3.8 or greater
* pip
* venv

Running the app with a Docker container:

* Docker

With all the prerequisites in hand you will need to update the `app.env` and `sample_secrets.env` file with your credentials 
and desired application settings. 

*Note: Do not check in your credentials into version control!! The .gitignore is already configured to ignore the file `secrets.env`
so I reccommend that you rename `sample_secrets.env` to `secrets.env` and then add your credentials to the file.*


## Running the Application

* [Running with Python and Flask](#running-with-python-and-flask)
* [Running with Docker](#running-with-docker)

### Running with Python and Flask

First clone this repository. 

```bash
git clone https://github.com/AlexanderJDupree/ImageGraph.git
```

Change into the `imagegraph` directory

```bash
cd ImageGraph/imagegraph
```

Then run one the runner scripts! 

```bash
./runner.sh # .\runner.bat for windows
```

This script will setup the virtual environment, download all the requirements and
then run the app. If you're using the default settings in `app.env` you'll be able 
to find the app at `http://localhost:5000`

### Running with Docker

First clone this repository. 

```bash
git clone https://github.com/AlexanderJDupree/ImageGraph.git
```

Change into the project root directory

```bash
cd ImageGraph
```

Build the image. *Note: Make sure your Google Credentials file and `secrets.env` file are not located in the `imagegraph` directory otherwise those files will be included in the built docker image. We will copy those required files into the created container in the next step*

```bash
docker build -f Dockerfile -t imagegraph .
```

Create the container for staging

```bash
docker create --name imagegraph-app -p 5000:5000 --env PORT=5000 -i imagegraph
```

Next copy your `secrets.env` and Google Application Credentials json file into the container

```bash
docker cp ${SECRETS_ENV_LOCATION}/secrets.env imagegraph-app:/secrets.env
docker cp ${CREDENTIAL_LOCATION}/credentials.json imagegraph-app:/credentials.json
```

Lastly, start the container! 

```
docker start imagegraph-app
```

## What's in this Repo?

```
.
├── Dockerfile
├── README.md
├── sample_secrets.env
│  
├── imagegraph
│   │
│   ├── db/             <-- DB Abstraction layer
│   │
│   ├── app.env         <-- Application settings
│   ├── app.py          <-- WSGI Flask App
│   ├── google_auth.py  <-- Google Oauth library
│   ├── requirements.txt
│   ├── runner.bat      <-- Runner scripts
│   ├── runner.sh
│   │
│   ├── static/         <-- Static resources, CSS, JS etc. 
│   └── templates/      <-- HTML templates
│
```
