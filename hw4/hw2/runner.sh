#!/bin/bash
# File      :   runner.sh
# Brief     :   bash script to setup virtualenv and run application
# Author    :   Alexander DuPree
# https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2

set -o errexit

if [[ ! -d "./env" ]] ; then
    python3 -m venv env
fi

if [[ -z "$VIRTUAL_ENV" ]] ; then
    source env/bin/activate
fi

pip install -r requirements.txt

python app.py
