"""
    :module: charities.models

    :brief: Implements the Data model for the Charity structure

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

class Service():
    '''Enumerated list of Charity Services'''
    NA                  = 'N/A'
    Education           = 'Education'
    Rehab               = 'Rehabilitation'
    Foodbank            = 'Foodbank'
    Homeless_Services   = 'Homeless Services'
    Animal_Rescue       = 'Animal Rescue'
    Social_Services     = 'Social Services'

    service_list = [NA, Education, Rehab, Foodbank, Homeless_Services, Animal_Rescue, Social_Services]


class Charity:
    '''Data model for Charities, mirrors SQl table structure'''

    def __init__(self, name, description, email, service, phone):
        '''Charities that aren't inserted into the DB will have None for the ID'''
        self.id             =   None
        self.name           =   name
        self.description    =   description
        self.email          =   email
        self.service        =   service
        self.phone          =   phone


    @classmethod
    def from_dict(cls, attr):

        charity = Charity(
            name=attr['name'], 
            description=attr['description'],
            email=attr['email'],
            service=attr['service'],
            phone=attr['phone'] )
        charity.id = attr['id']
        return charity
