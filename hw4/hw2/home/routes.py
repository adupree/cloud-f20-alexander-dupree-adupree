"""
    :module:    home.routes

    :brief:     Blueprint for the charities home module of the Charity Connect
        web application

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from flask import Blueprint
from home.index import Index
from home.about import About

home = Blueprint('home', __name__)

home.add_url_rule('/', view_func=Index.as_view('index'))
home.add_url_rule('/about', view_func=About.as_view('about'))
