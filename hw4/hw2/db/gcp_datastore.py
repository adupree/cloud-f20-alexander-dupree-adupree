"""
    :module: db.gcp_datastore

    :brief: Implements the database model as a Google Cloud Datastore

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw4
"""

import sqlite3
from datetime import datetime
from google.cloud import datastore
from db.models import DB_Model
from charities.models import Charity

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.
    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]
    This returns:
        [ name, email, date, message ]
    where name, email, and message are Python strings
    and where date is a Python datetime
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return Charity.from_dict(entity)

class GCP_Datastore(DB_Model):
    ''' Database model implementation with a GCP Datastore backend'''

    ENTITY_KIND = 'Charity'

    def __init__(self, config):
        '''Constructor instantiates a GCP datastore client
        '''
        super(GCP_Datastore, self).__init__(config)
        self.client = datastore.Client(config.get('PROJECT_ID'))


    def add_charity(self, charity):
        ''' Add Charity object to database

        :param charity: Charity ORM
        :type charity:  charities.models.Charity

        :returns: The charity object arguments with the ID field updated 
        '''
        key = self.client.key(GCP_Datastore.ENTITY_KIND, charity.name)
        entry = datastore.Entity(key)

        charity.id = entry.id # Update the id number of the charity

        entry.update(charity.__dict__)
        self.client.put(entry)
        return charity


    def get_charity(self, charity_name):
        '''Retrieve a Charity ORM from the Database

        :param charity_name: Name of the Charity to look for
        :type charity_name: str

        :returns: Either the Charity object or Nothing
        :rtype: None or charities.models.Charity
        '''
        key = self.client.key(GCP_Datastore.ENTITY_KIND, charity_name)
        return from_datastore(self.client.get(key))


    def get_all_charities(self):
        '''Retrieve a list of all Charities from the Database

        :returns: Charity objects in a list
        :rtype: [charities.models.Charity]
        '''
        query = self.client.query(kind=GCP_Datastore.ENTITY_KIND)
        entities = list(map(from_datastore,query.fetch()))
        return entities


    def delete_charity(self, charity_id):
        '''Remove a Charity from the database

        :param charity_id: ID of the charity to remove
        :type charity_id: int
        '''
        key = self.client.key(GCP_Datastore.ENTITY_KIND, charity_id)
        self.client.delete(key)

    def close(self):
        '''No teardown needed for GCP Datastore'''
        return
