"""
    :module: db.sqlite3_db

    :brief: Implements the database model as a sqlite3 database

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

import sqlite3
from db.models import DB_Model
from charities.models import Charity


class Sqlite3_DB(DB_Model):
    ''' Database model implementation with a sqlite3 backend'''

    def __init__(self, config):
        '''Constructor connects to the database and initializes the tables
        if needed
        '''

        super(Sqlite3_DB, self).__init__(config)

        self.conn = sqlite3.connect( 
            config.get('DB_URI', ':memory:'),
            detect_types=sqlite3.PARSE_DECLTYPES
            )

        self.conn.row_factory = sqlite3.Row

        with open('db_schema.sql') as f:
            self.conn.executescript(f.read())


    def add_charity(self, charity):
        ''' Add Charity object to database

        :param charity: Charity ORM
        :type charity:  charities.models.Charity

        '''
        cursor = self.conn.cursor()

        cursor.execute(
            'INSERT INTO Charities (name, description, email, service, phone) VALUES (?, ?, ?, ?, ?)', (
                charity.name, 
                charity.description, 
                charity.email, 
                charity.service, 
                charity.phone
                )
            )

        charity.id = cursor.lastrowid
        self.conn.commit()
        return charity


    def get_charity(self, charity_name):
        '''Retrieve a Charity ORM from the Database

        :param charity_name: Name of the Charity to look for
        :type charity_name: str

        :returns: Either the Charity object or Nothing
        :rtype: None or charities.models.Charity
        '''

        cursor = self.conn.cursor()

        result = cursor.execute('SELECT * FROM Charities WHERE name=?', (charity_name,)).fetchone()

        return result if result is None else Charity.from_dict(result)


    def get_all_charities(self):
        '''Retrieve a list of all Charities from the Database

        :returns: Charity objects in a list
        :rtype: [charities.models.Charity]
        '''
        cursor = self.conn.cursor()

        cursor.execute('SELECT * FROM Charities ORDER BY created_at DESC')

        return list(map(Charity.from_dict, cursor.fetchall()))


    def delete_charity(self, charity_id):
        '''Remove a Charity from the database

        :param charity_id: ID of the charity to remove
        :type charity_id: int
        '''

        cursor = self.conn.cursor()

        cursor.execute('DELETE FROM Charities WHERE id=?', (charity_id,))

        self.conn.commit()
        return cursor.rowcount


    def close(self):
        '''Closes the DB connection'''
        self.conn.commit()
        self.conn.close()
