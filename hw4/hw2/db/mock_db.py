"""
    :module: db.mock_db

    :brief: Implements the database model as a python dictionary

    :author: Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree/hw2
"""

from db.models import DB_Model

class Mock_DB(DB_Model):

    def __init__(self, config):
        super(Mock_DB, self).__init__(config)
        self.db = {}


    def add_charity(self, charity):
        charity.id = max(self.db.keys())
        self.db[charity.id] = charity


    def get_charity(self, charity_name):
        return list(filter(lambda x: x.name == charity_name, self.db.values()))


    def get_all_charities(self):
        return self.db.values()


    def delete_charity(self, charity_id):
        return True if self.db.pop(charity_id, False) else False


    def close(self):
        return
    
