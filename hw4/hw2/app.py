"""
    :module:    app.py

    :brief:     Entry point for the Charity Connect web application

    :author:    Alexander DuPree

    https://gitlab.com/adupree/cloud-f20-alexander-dupree-adupree
"""

from os import environ, urandom

from flask import Flask, g

from db import init_db
from home.routes import home
from charities.routes import charities

app = Flask(__name__)

app.register_blueprint(home)
app.register_blueprint(charities)

@app.teardown_appcontext
def teardown_db(exception):
    """Closes open DB resources if it exists
    """
    db = g.pop('db', None)

    if db is not None:
        db.close()

if __name__ == '__main__':

    config = environ.get('CC_CONFIG', 'default.cfg')

    app.config.from_pyfile(config)

    app.config['SECRET_KEY'] = environ.get('SECRET_KEY', urandom(12))

    init_db(app.config)

    app.run(host=app.config['HOSTNAME'], port=app.config['PORT'], debug=app.config['DEBUG'])